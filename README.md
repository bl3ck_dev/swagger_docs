REST FRAMEWoRK API 

IMPLEMENTED with 
- jwt authentication
- Swagger docs
- redoc 


TO USE
1. clone repo
2. run 'py -m venv env' to create virtual environment
3. activate virtual env
4. run 'pip install -r req.txt'
5. makemigrations and migrate
6. run 'py manage.py runserver'