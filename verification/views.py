from django.shortcuts import render
from rest_framework.generics import GenericAPIView, ListCreateAPIView,ListAPIView
from .serializer import UserSerializer, LoginSerializer, UserDetailsSerializer
from datetime import datetime, timedelta
from rest_framework.response import Response
from rest_framework import status
import jwt
from django.contrib import auth
from django.contrib.auth.models import User
# Create your views here.


class RegisterView(GenericAPIView):
    serializer_class = UserSerializer
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class UserDetails(ListAPIView):
    serializer_class = UserDetailsSerializer

    def get_queryset(self):
        return User.objects.filter(username=self.request.user.username)


      
class LoginView(GenericAPIView):
    serializer_class = LoginSerializer
    def post(self, request):
        key = 'secret'
        data =  request.data
        username = data.get('username', '')
        password = data.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user:
            auth_token = jwt.encode( 
                {"username": user.username,
                "exp": datetime.utcnow() + timedelta(seconds=600)},
                 key, algorithm='HS256' )
            serializer = UserSerializer(user)
            data = {
                'user': serializer.data,
                'token': auth_token,
            }
            return Response(data, status= status.HTTP_200_OK)


        return Response({'detail':'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)


        